package lootweb

import (
	"log"
	"strconv"
	"testing"

	"gitlab.com/ramencatz/golang/lootengine"
	"gitlab.com/ramencatz/projects/arpg/modules/dynamodb"
)

func TestExampleResult(t *testing.T) {
	var lootItems []lootengine.Root

	lootItems = append(lootItems, lootengine.NewDefaultItem("Bacon"))
	lootItems = append(lootItems, lootengine.NewDefaultItem("Eggs"))
	lootItems = append(lootItems, lootengine.NewDefaultItem("Toast"))

	result := ExampleResult("Testing Result", lootItems)

	log.Default().Println(result)

}

func TestExampleTableData(t *testing.T) {

	var lootItems []dynamodb.DBFullItem

	//build a basic list
	for i := 0; i < 10; i++ {
		itemStr := strconv.Itoa(i)
		lootItem := dynamodb.DBFullItem{
			ItemName:    "Name " + itemStr,
			Category:    "Category " + itemStr,
			Description: "Some description",
			ImageName:   itemStr,
			Material:    "Cosmic",
			Occasion:    []string{"Market", "Quest"},
			Probability: "Common",
			Tier:        "Low",
		}

		lootItems = append(lootItems, lootItem)

	}

	result := ExampleTableData("My title", lootItems)

	log.Default().Println(result)

}
