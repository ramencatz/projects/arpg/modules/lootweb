package lootweb

import (
	"bytes"
	"html/template"

	"gitlab.com/ramencatz/golang/lootengine"
	"gitlab.com/ramencatz/projects/arpg/modules/dynamodb"
)

var header = `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{{.}}</title>
	</head>
	<style>
		table {
		font-family: arial, sans-serif;
		border-collapse: collapse;

		}

		td, th {
		border: 1px solid #dddddd;
		text-align: left;
		padding: 8px;
		}

		tr:nth-child(even) {
		background-color: #dddddd;
		}
	</style>
	<body>
	`

var pageTitle = `
	<h1>{{.}}</h1>
	`

var tableOfItems = `
	<table>
	<tr>
	<th>Quantity</th><th>Item</th>
	</tr>
	{{ range .}}
		<tr><td>{{ .Quantity}}</td><td>{{ .Name }}</td></tr>
	{{ end}}
	</table>

`

var tableOfCompleteItemData = `
	<table>
	<tr>
	<th>Item Name</th><th>Category</th><th>Description</th>
	    <th>Image Name</th><th>Material</th>
		<th>Occasion</th><th>Probability</th><th>Tier</th>
	</th>
	</tr>
	{{ range . }}
		<tr>
			<td>{{ .ItemName }}</td><td>{{ .Category}}</td>
			<td>{{ .Description }}</td><td>{{ .ImageName}}</td>
			<td>{{ .Material }}</td><td>{{ .Occasion}}</td>
			<td>{{ .Probability }}</td><td>{{ .Tier}}</td>
		</tr>
	{{ end}}
	</table>

`

var footer = `
	</body>
</html>`

// Show the loot drop result.  Provides the quanity and item name
// in a html page with a table.
func ExampleResult(title string, items []lootengine.Root) string {

	headerTemplate := template.Must(template.New("header").Parse(header))
	titleTemplate := template.Must(template.New("title").Parse(pageTitle))
	tableTemplate := template.Must(template.New("tableOfItems").Parse(tableOfItems))
	footerTemplate := template.Must(template.New("footer").Parse(footer))

	var bytesBuffer bytes.Buffer

	headerTemplate.Execute(&bytesBuffer, title)
	titleTemplate.Execute(&bytesBuffer, title)
	tableTemplate.Execute(&bytesBuffer, items)
	footerTemplate.Execute(&bytesBuffer, "")

	return bytesBuffer.String()
}

// Show the complete item data
func ExampleTableData(title string, items []dynamodb.DBFullItem) string {

	headerTemplate := template.Must(template.New("header").Parse(header))
	titleTemplate := template.Must(template.New("title").Parse(pageTitle))
	tableTemplate := template.Must(template.New("tableOfCompleteItemData").Parse(tableOfCompleteItemData))
	footerTemplate := template.Must(template.New("footer").Parse(footer))

	var bytesBuffer bytes.Buffer

	headerTemplate.Execute(&bytesBuffer, title)
	titleTemplate.Execute(&bytesBuffer, title)
	tableTemplate.Execute(&bytesBuffer, items)
	footerTemplate.Execute(&bytesBuffer, "")

	return bytesBuffer.String()
}
